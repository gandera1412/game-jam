using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LvManager : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] GameObject[] gameObjects;
    [SerializeField] Blade blade;
    [HideInInspector] public int CurrentLv = 1;
    [HideInInspector] public int pointtochangelv;
    float timer = 4;
    [SerializeField] Text textlv;
    [SerializeField] Text nextlv;
    [SerializeField] VisualizerScript visualizerScript;
    [SerializeField] GameObject pnlInGame;
    [SerializeField] GameObject pnlIGameOver;
    [SerializeField] GameObject SoundPlay;
    [SerializeField] Text textYourScore;
    [SerializeField] Text TextBestScore;
    [SerializeField] Text TextBestScoreinGame;
    [SerializeField] GameObject MainMenubtn;
    [SerializeField] Text CountingDown;
    [SerializeField] float TimeCountingDown = 241;
    int bestScore;

    void Start()
    {
        Screen.SetResolution(1920, 1080, false);
        bestScore = PlayerPrefs.GetInt("BestScore");
        pointtochangelv = gameObjects[0].transform.GetChild(0).transform.GetChildCount();
        textlv.text = "LEVEL 1";
        nextlv.gameObject.SetActive(false);
        pnlInGame.gameObject.SetActive(true);
        pnlIGameOver.gameObject.SetActive(false);
        MainMenubtn.gameObject.SetActive(false);
        SoundPlay.gameObject.SetActive(true);
        TextBestScoreinGame.text = "Best: " +bestScore.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        TimeCountingDown -= Time.deltaTime;
        CountingDown.text = ((int)TimeCountingDown / 60).ToString()+":" + ((int)TimeCountingDown % 60).ToString();
        if(TimeCountingDown <=0)
        {
            EndGame();
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            Change_Lv(); // trick test
            textlv.text = "LEVEL " + CurrentLv.ToString();
        }
        if(pointtochangelv <= 0)
        {
            timer -= Time.deltaTime;
            if(CurrentLv != 6)
            {
                gameObjects[CurrentLv - 1].gameObject.SetActive(false);
                nextlv.gameObject.SetActive(true);
            }
            if(CurrentLv == 6)
            {
                EndGame();
            }
            
        }
        if(timer >1 )
        {
            nextlv.text = ((int)timer).ToString();
        }
        else
        {
            nextlv.text = "Start";
        }
        if (timer < 0)
        {
            nextlv.gameObject.SetActive(false);
            Change_Lv();
            textlv.text = "LEVEL "+ CurrentLv.ToString();
            timer = 4;
        }
    }
    public void Change_Lv()
    {
        CurrentLv++;
        for(int i = 0; i < 6; i++)
        {
            if(CurrentLv-1 == i)
            {
                gameObjects[i].gameObject.SetActive(true);
                if(i!= 5)
                {
                    pointtochangelv = 2*(gameObjects[i].transform.GetChild(0).transform.GetChildCount());
                }
                if (i ==5)
                {
                    pointtochangelv = 1752;
                }
            }
            else
            {
                gameObjects[i].gameObject.SetActive(false);
            }
        }
        if(CurrentLv == 7)
        {    
            EndGame();
        }
    }
    public void EndGame()
    {
        visualizerScript.turnoffAudio();
        if (blade.point > bestScore)
        {
            PlayerPrefs.SetInt("BestScore", blade.point);
        }
        pnlInGame.gameObject.SetActive(false);
        pnlIGameOver.gameObject.SetActive(true);
        MainMenubtn.gameObject.SetActive(true);
        SoundPlay.gameObject.SetActive(false);
        textYourScore.text = blade.point.ToString();
        TextBestScore.text =( PlayerPrefs.GetInt("BestScore")).ToString();
    }
}
