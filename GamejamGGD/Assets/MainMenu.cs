using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    private void Start()
    {
        Screen.SetResolution(1920, 1080, false);
    }
    public void StartBtn()
    {
        SceneManager.LoadScene(1);
        //Debug.Log(2);
    }
    public void MainMenubtn()
    {
        SceneManager.LoadScene(0);
        Debug.Log(2);
    }
    public void Quitbtn()
    {
        Application.Quit();
        Debug.Log(2);
    }
}
