using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vfx : MonoBehaviour
{
    // Start is called before the first frame update
    // Start is called before the first frame update
    public Rigidbody piece;
    public float shootPower = 1000f;

    //Update is called once per frame
    private void Start()
    {
        piece = GetComponent<Rigidbody>();
        int x = Random.Range(2, -2);
        piece.velocity = new Vector3(x, 2,0) * shootPower;
    }

}
