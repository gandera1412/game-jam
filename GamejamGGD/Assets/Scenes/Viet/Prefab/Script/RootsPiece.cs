using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootsPiece : MonoBehaviour
{
    // Start is called before the first frame update
    [HideInInspector] public int CurrentStatus = 0;
    public Material[] materials;
    Renderer rend;
    // 0 black, 1 grey, 2 white, 3 red
    void Start()
    {
        rend = GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = materials[CurrentStatus];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Change_Material()
    {
        if(CurrentStatus < 3)
        {
            CurrentStatus++;
           
        }
        gameObject.transform.GetChild(CurrentStatus - 1).gameObject.SetActive(true);

        //Debug.Log(CurrentStatus - 1);
        //rend.sharedMaterial = materials[CurrentStatus];
    }
}
