using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratePiece : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] GameObject piece;
    [SerializeField] Transform parent;
    [SerializeField] float smooth;
    [SerializeField] float numpice;
    new Vector3 StarPointPos;
    void Start()
    {
        StarPointPos = transform.position;
        for (int x = 0; x <= numpice; x++)
        {
            float xx = 0 + x * smooth;
            for (int y = 0; y <= numpice; y++)
            {
                float yy = 0 + y * smooth;
                Instantiate(piece.gameObject, transform.position + new Vector3(xx, yy, 0), Quaternion.identity).transform.SetParent(parent);
            }
               
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
