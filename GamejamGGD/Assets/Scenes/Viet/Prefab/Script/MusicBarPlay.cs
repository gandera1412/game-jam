using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicBarPlay : MonoBehaviour
{
    // Start is called before the first frame update
    public float seconds = 10;
    [HideInInspector]public float timer;
    [HideInInspector] public Vector3 Point;
    [HideInInspector] public Vector3 Difference;
    [HideInInspector] public Vector3 start;
    [HideInInspector] public float percent;
    [SerializeField] Blade bl;
    void Start()
    {
        start = transform.position;
        Point = transform.position + new Vector3(0, -14, 0);
        Difference = Point - start;
        //bl.start = true;
    }

    void Update()
    {

        if (timer <= seconds)
        {
            // basic timer
            timer += Time.deltaTime;
            percent = timer / seconds;
            transform.position = start + Difference * percent;
        }
        if(timer > seconds)
        {
            Vector3 temp = start;
            start = Point;
            Point = temp;
            Difference = Point - start;
            timer = 0;
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "PlayBar")
        {
           //bl.start = false;
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "PlayBar")
        {
           // bl.start = true;
        }
    }
}
