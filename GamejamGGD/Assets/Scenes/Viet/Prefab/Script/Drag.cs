using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drag : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject selectedObject;
    private GameObject doObject;


    Vector3 trasnpos = new Vector3();
    float trasnx;
    float trasny;
    bool check;
    //l?y v? tr ban ??u c?a gameobject ?? so snh v?i v? tr c?a con tr? chu?t


    int dropx;
    int dropy;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (selectedObject == null)
            {
                check = true;
                RaycastHit hit = CastRay();

                if (hit.collider != null)
                {
                    if (!hit.collider.CompareTag("drag"))
                    {
                        return;
              
                    }
                    trasnpos = hit.collider.gameObject.transform.position;

                    selectedObject = hit.collider.gameObject;
                    selectedObject.gameObject.tag = "drag";

                    //var ispasssreen = selectedObject.GetComponent<Block>();
                    //ispasssreen.ispasssreen = false;
                    Cursor.visible = false;
                }
            }
            if (selectedObject != null)
            {
                selectedObject.gameObject.tag = "drag";
                var blade = selectedObject.GetComponent<Blade>();
                blade.ChangHand();

            }

        }
        if (selectedObject != null)
        {
            Vector3 position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.WorldToScreenPoint(selectedObject.transform.position).z);
            Vector3 worldPosotion = Camera.main.ScreenToWorldPoint(position);
            if (check == true)
            {
                trasnx = trasnpos.x - worldPosotion.x;
                trasny = trasnpos.y - worldPosotion.y;
                check = false;

            }
            Blade blade = selectedObject.GetComponent<Blade>();
            selectedObject.transform.position = new Vector3(worldPosotion.x + trasnx, worldPosotion.y + trasny, -0.5f) + blade.ShakePosition;


            if (Input.GetMouseButtonUp(0))
            {
                selectedObject.gameObject.tag = "nil";
                blade.ChangHand();
                //selectedObject.transform.position = new Vector3(worldPosotion.x + trasnx, worldPosotion.y + trasny, -0.5f);
                ////doPosition();
                //doObject = selectedObject;
                //selectedObject = null;
                Cursor.visible = true;
            }
        }

    }
    private RaycastHit CastRay()
    {
        Vector3 screenMousePosFar = new Vector3(
            Input.mousePosition.x,
            Input.mousePosition.y,
            Camera.main.farClipPlane);
        Vector3 screenMousePosNear = new Vector3(
            Input.mousePosition.x,
            Input.mousePosition.y,
            Camera.main.nearClipPlane);
        Vector3 worldMousePosFar = Camera.main.ScreenToWorldPoint(screenMousePosFar);
        Vector3 worldMousePosNear = Camera.main.ScreenToWorldPoint(screenMousePosNear);
        RaycastHit hit;
        Physics.Raycast(worldMousePosNear, worldMousePosFar - worldMousePosNear, out hit);
        return hit;
    }
}
